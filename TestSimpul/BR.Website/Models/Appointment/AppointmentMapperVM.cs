﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BR.BLL.Appointment.Models;

namespace BR.Website.Models.Appointment
{
    public class AppointmentMapperVM : Profile
    {
        public AppointmentMapperVM()
        {
            CreateMap<AppointmentDto, AppointmentVM>().ReverseMap();
        }
    }
}
