﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Website.Models.Appointment
{
    public class AppointmentVM
    {
        public Guid id { get; set; }
        public DateTime requestdate { get; set; }
        public string petname { get; set; }
        public string ownername { get; set; }
        public string contactperson { get; set; }
        public DateTime createddate { get; set; }

    }
}
