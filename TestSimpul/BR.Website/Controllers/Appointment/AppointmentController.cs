﻿using BR.DataHelpers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using BR.Website.Controllers;
using Microsoft.AspNetCore.Hosting;
using System.Security.Cryptography;
using System.Text;
using BR.Website.Models.Appointment;
using App.BLL.Appointment.Logics;
using BR.BLL.Appointment.Models;

namespace BR.Website.Controllers
{
    public class AppointmentController : CoreController
    {

        public IActionResult Index()
        {
            bool success = true;
            List<string> errors = new List<string>();
            List<AppointmentVM> model = null;
            try
            {
                var svc = Service<AppointmentBLL>();
                var getResult = svc.GetAll();
                if (!getResult.Status.IsSuccess)
                {
                    success = false;
                    errors = getResult.Status.Errors;
                }
                model = (getResult.Status.IsSuccess) ? Mapper.Map<List<AppointmentDto>, List<AppointmentVM>>(getResult.Data.ToList()) : new List<AppointmentVM>();
            }
            catch (Exception ex)
            {
                success = false;
                errors.Add(ex.Message);
            }

            return View(model); 
        }

        
    }
}