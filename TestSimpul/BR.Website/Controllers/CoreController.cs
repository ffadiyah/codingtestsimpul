﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BR.DAL.Models;
using BR.DataHelpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using System.Security.Claims;

namespace BR.Website.Controllers
{
    public class CoreController : Controller
    {
        protected virtual ViewResult PrintError(IEnumerable<string> errors)
        {
            return View("Error", errors);
        }
        protected virtual ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        
        protected virtual string GetCurrentUser()
        {
            try
            {
                return HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;
            }
            catch
            {
                return string.Empty;
            }
        }

        protected virtual string GetCurrentUserID()
        {
            try
            {
                return HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
            }
            catch
            {
                return string.Empty;
            }
        }

        protected virtual string GetCurrentRole()
        {
            try
            {
                return HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Actor).Value;
            }
            catch
            {
                return string.Empty;
            }
        }

        protected BRContext DbContext
        {
            get
            {
                return ServiceProviderHelper.Service<BRContext>(HttpContext);
            }
        }

        protected T Service<T>() where T : ServiceBase
        {
            return (T)Activator.CreateInstance(typeof(T), DbContext);
        }
    }
}