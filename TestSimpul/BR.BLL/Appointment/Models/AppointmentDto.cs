﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BR.BLL.Appointment.Models
{
    public class AppointmentDto
    {
        public Guid id { get; set; }
        public DateTime requestdate { get; set; }
        public string petname { get; set; }
        public string ownername { get; set; }
        public string contactperson { get; set; }
        public DateTime createddate { get; set; }
    }
}
