﻿using BR.DAL.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using BR.BLL.Appointment.Models;

namespace BR.BLL.Appointment.MapperProfile
{
    public class AppointmentMapper : Profile
    {
        public AppointmentMapper()
        {
            CreateMap<appointment, AppointmentDto>().ReverseMap();
        }
    }
}
