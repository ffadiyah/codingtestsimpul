﻿using BR.DAL.Models;
using BR.DataHelpers;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using BR.BLL.Appointment.Models;

namespace App.BLL.Appointment.Logics
{
    public class AppointmentBLL : ServiceBase, IDisposable
    {
        UnitOfWork _UnitOfWork;
        IRepository<appointment> _repoAppointment;

        public AppointmentBLL(BRContext _DbContext) : base(_DbContext)
        {
            _UnitOfWork = new UnitOfWork(_DbContext);
            _repoAppointment = _UnitOfWork.Repository<appointment>();
        }

        public DataResult<IEnumerable<AppointmentDto>> GetAll(Expression<Func<appointment, bool>> expr = null, Func<IQueryable<appointment>, IOrderedQueryable<appointment>> order = null)
        {
            bool success = true;
            List<string> errors = new List<string>();
            IEnumerable<AppointmentDto> result = null;
            try
            {
                var getResult = _repoAppointment.GetAll();
                if (!getResult.Status.IsSuccess)
                {
                    return new DataResult<IEnumerable<AppointmentDto>>(null, getResult.Status);
                }
                result = Mapper.Map<IEnumerable<appointment>, IEnumerable<AppointmentDto>>(getResult.Data);
            }
            catch (Exception ex)
            {
                success = false;
                errors.Add(ex.Message);
            }
            return new DataResult<IEnumerable<AppointmentDto>>(result, new StatusResult(success, errors));
        }

        public DataResult<AppointmentDto> Get(Expression<Func<appointment, bool>> expression = null)
        {
            bool success = true;
            List<string> errors = new List<string>();
            AppointmentDto data = null;
            try
            {
                DataResult<appointment> getResult = _repoAppointment.Get(expression);
                if (!getResult.Status.IsSuccess)
                {
                    return new DataResult<AppointmentDto>(null, getResult.Status);
                }
                data = Mapper.Map<appointment, AppointmentDto>(getResult.Data);
            }
            catch (Exception ex)
            {
                success = false;
                errors.Add(ex.Message);
            }
            return new DataResult<AppointmentDto>(data, new StatusResult(success, errors));
        }

        public DataResult<int> Insert(AppointmentDto data)
        {
            IRepository<appointment> repository = _UnitOfWork.Repository<appointment>();
            appointment entity = Mapper.Map<AppointmentDto, appointment>(data);
            var insertResult = repository.Insert(entity);
            if (!insertResult.Status.IsSuccess)
                return new DataResult<int>(0, insertResult.Status);
            var saveResult = _UnitOfWork.Save();
            return new DataResult<int>(insertResult.Data, saveResult);
        }

        public StatusResult Update(AppointmentDto data)
        {
            appointment entity = Mapper.Map<AppointmentDto, appointment>(data);
            IRepository<appointment> repository =
                _UnitOfWork.Repository<appointment>();
            var result = repository.Update(entity);
            if (!result.IsSuccess)
            {
                return result;
            }
            return _UnitOfWork.Save();
        }
        public DataResult<bool> Exists(Expression<Func<appointment, bool>> expression)
        {
            bool success = false;
            List<string> errors = new List<string>();
            bool exists = false;
            try
            {

                var checkResult = _repoAppointment.Exists(expression);
                return checkResult;
            }
            catch (Exception ex)
            {
                success = false;
                errors.Add(ex.Message);
            }
            return new DataResult<bool>(exists, new StatusResult(success, errors));
        }
    }
}
