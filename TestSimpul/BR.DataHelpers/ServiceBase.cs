﻿using BR.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.DataHelpers
{
    public class ServiceBase
    {
        private readonly BRContext _Db;
        public ServiceBase(BRContext db)
        {
            _Db = db;
        }
        protected BRContext Db
        {
            get
            {
                return _Db;
            }
        }

        public void Dispose()
        {
            try
            {
                _Db.Dispose();
            }
            catch (Exception)
            {
            }
        }
    }
}
