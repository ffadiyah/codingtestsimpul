﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BR.DataHelpers
{
    public class StatusResult
    {

        public StatusResult(bool isSuccess, List<string> errors, int Id = 0)
        {
            IsSuccess = isSuccess;
            Errors = errors;
            ID = Id;
        }
      
        public bool IsSuccess { get; set; }
        public List<string> Errors { get; set; }
        public int ID { get; set; }
    }
}
