﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.DataHelpers
{
    public class ServiceProviderHelper
    {
        public static T Service<T>(HttpContext context)
        {
            var serviceProvider = context.RequestServices;
            var x = (T)serviceProvider.GetService(typeof(T));
            return x;
        }
    }
}
