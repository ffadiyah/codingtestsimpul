﻿using System;

namespace BR.DataHelpers
{
    public class DataResult <T>
    {
        public DataResult(T data, StatusResult status, int Id = 0)
        {
            Data = data;
            Status = status;
            ID = Id;
        }

        public T Data { get; set; }
        public StatusResult Status { get; private set; }
        public int ID { get; set; }
    }
}
