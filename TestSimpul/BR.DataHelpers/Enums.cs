﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;

namespace BR.DataHelpers
{
    public static class Enums
    {
        public enum LandingMenu
        {
            [Description("Dashboard")]
            Dashboard,
            [Description("InitialData")]
            InitialData,
            [Description("Cases")]
            Cases,
            [Description("Evidences")]
            Evidences,
            [Description("Report")]
            Report,
            [Description("Setting")]
            Setting,
        }

        public enum NamaMenu
        {
            [Description("Master Gedung")]
            MasterGedung,
            [Description("Status Kasus")]
            StatusKasus,
            [Description("Kasus")]
            Kasus,
        }

        public enum ActionForm
        {
            [Description("Add")]
            Add,
            [Description("Edit")]
            Edit,
            [Description("View")]
            View,
            [Description("Delete")]
            Delete,
            [Description("Print")]
            Print,
            [Description("Download")]
            Download,
        }

        public enum RoleMenu
        {
            [Description("View")]
            View,
            [Description("Insert")]
            Insert,
            [Description("Update")]
            Update,
            [Description("Delete")]
            Delete,
            [Description("Print")]
            Print,
            [Description("Approve")]
            Approve,
            [Description("Reject")]
            Reject,
            [Description("Download")]
            Download,
            [Description("Upload")]
            Upload,
        }

        public enum UserRole
        {
            [Description("Resepsionis")]
            Resepsionis,
            [Description("Kepala Lab")]
            KepalaLab,
            [Description("Kepala Tim")]
            KepalaTim,
            [Description("Petugas")]
            Petugas
        }

        public enum StatusKasus
        {
            [Description("Draft")]
            Draft,
            [Description("Menunggu Persetujuan")]
            MenungguPersetujuan,
            [Description("Disetujui")]
            Disetujui,
            [Description("Ditolak")]
            Ditolak,
            [Description("Dalam Proses")]
            OnProgress,
            [Description("Selesai")]
            Complete,
            [Description("Diarsipkan")]
            Diarsipkan,
            [Description("Ditutup")]
            Ditutup,
        }

        public enum StatusBarangBukti
        {
            [Description("Penerimaan")]
            Penerimaan = 1,
            [Description("Diproses")]
            Diproses = 2,
            [Description("Keluar Gudang")]
            KeluarGudang = 3,
            [Description("Kembali ke Gudang")]
            KembalikeGudang = 4,
            [Description("Dipinjam")]
            Dipinjam = 5,
            [Description("Kembali ke Pemilik")]
            KembalikePemilik = 6,
            [Description("Dimusnahkan")]
            Dimusnahkan = 7,
            [Description("Proses Dikembalikan ke Pemilik")]
            DisetujuiDikembalikankePemilik = 8,
            [Description("Disetujui Dimusnahkan")]
            DisetujuiDimusnahkan = 9,
            [Description("Draft")]
            Draft = 10,
            [Description("Menunggu Persetujuan")]
            MenungguPersetujuan = 11,
            [Description("Disetujui")]
            Disetujui = 12,
            [Description("Ditolak")]
            Ditolak = 13,
            [Description("Tutup Kasus")]
            TutupKasus = 14
        }

        public enum StatusCoc
        {
            [Description("Disetujui")]
            Disetujui,
            [Description("Menunggu Persetujuan")]
            MenungguPersetujuan,
            [Description("Ditolak")]
            Ditolak
        }

        public enum JenisAktivitas
        {
            [Description("Dipinjam")]
            Dipinjam,
            [Description("Dimusnahkan")]
            Dimusnahkan,
            [Description("Dikembalikan")]
            Dikembalikan
            
        }
        public enum StatusTugas
        {
            [Description("To Do")]
            ToDo,
            [Description("On Progress")]
            OnProgress,
            [Description("Finished")]
            Finished
        }
        public enum KasusMenu
        {
            [Description("PencatatanKasus")]
            PencatatanKasus,
            [Description("BarangBukti")]
            BarangBukti,
            [Description("Lampiran")]
            Lampiran,
            [Description("TahapanKerja")]
            TahapanKerja,
            [Description("DaftarTugas")]
            DaftarTugas
        }

        public enum BuktiMenu
        {
            [Description("Peminjaman")]
            Peminjaman,
            [Description("Pengembalian")]
            Pengembalian,
            [Description("Pemusnahan")]
            Pemusnahan
        }

        public enum SettingMenu
        {
            [Description("ProfilePerusahaan")]
            ProfilePerusahaan,
            [Description("Menu")]
            Menu,
            [Description("User")]
            User,
            [Description("Role")]
            Role,
            [Description("FormatKode")]
            FormatKode
        }

        public enum MasterMenu
        {
            [Description("User")]
            User,
            [Description("Gedung")]
            Gedung,
            [Description("MasterRak")]
            MasterRak,
            [Description("StatusKasus")]
            StatusKasus,
            [Description("MasterJob")]
            MasterJob,
            [Description("Petugas")]
            Petugas,
            [Description("GroupPetugas")]
            GroupPetugas,
            [Description("PenyidikClient")]
            PenyidikClient,
            [Description("Hirarki")]
            Hirarki,
            [Description("Tools")]
            Tools,
            [Description("Lookup")]
            Lookup,
            [Description("PaketLayanan")]
            PaketLayanan
        }

        public enum ButtonForm
        {
            [Description("Ajukan Kasus")]
            AjukanKasus,
            [Description("Edit")]
            Edit,
            [Description("Arsipkan")]
            Arsipkan,
            [Description("Tutup Kasus")]
            TutupKasus,
            [Description("Detail")]
            Detail,
            [Description("Tambah Barang Bukti")]
            TambahBarangBukti,
            [Description("Tugas")]
            Tugas,
            [Description("Generate Report")]
            GenerateReport,
            [Description("Change Status")]
            ChangeStatus
        }
        public enum LevelBarangBukti
        {
            [Description("Barang Bukti Utama")]
            BarangBuktiUtana = 1,
            [Description("Barang Bukti Turunan")]
            BarangBuktiTurunan = 2,
            //[Description("Barang Bukti Kedua")]
            //BarangBuktiKedua = 2,
            //[Description("Barang Bukti Ketiga")]
            //BarangBuktiKetiga = 3,
        }

        public enum KodeTransaksi
        {
            [Description("PK")]
            Kasus,
            [Description("BB")]
            BarangBukti,
            [Description("T")]
            Tugas
        }
        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            string description = null;

            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                        if (descriptionAttributes.Length > 0)
                        {
                            // we're only getting the first description we find
                            // others will be ignored
                            description = ((DescriptionAttribute)descriptionAttributes[0]).Description;
                        }

                        break;
                    }
                }
            }

            return description;
        }

    }
}
