﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BR.DAL.Models
{
    public class BRContext : DbContext
    {
        public BRContext(DbContextOptions<BRContext> options) : base(options)
        {

        }

        public virtual DbSet<appointment> appointment { get; set; }
        
    }
}
