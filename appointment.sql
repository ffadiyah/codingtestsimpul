/*
 Navicat Premium Data Transfer

 Source Server         : pg-bounga
 Source Server Type    : PostgreSQL
 Source Server Version : 110002
 Source Host           : dev.bounga.cikaso.com:5400
 Source Catalog        : bounga-rampaii-dev
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110002
 File Encoding         : 65001

 Date: 11/11/2021 20:41:45
*/


-- ----------------------------
-- Table structure for appointment
-- ----------------------------
DROP TABLE IF EXISTS "public"."appointment";
CREATE TABLE "public"."appointment" (
  "id" uuid NOT NULL,
  "requestdate" date,
  "petname" varchar(100) COLLATE "pg_catalog"."default",
  "ownername" varchar(100) COLLATE "pg_catalog"."default",
  "contactperson" varchar(50) COLLATE "pg_catalog"."default",
  "createddate" timestamp(6)
)
;

-- ----------------------------
-- Records of appointment
-- ----------------------------
INSERT INTO "public"."appointment" VALUES ('1fe5d1c7-fa16-424d-907a-5a121d375e9d', '2021-11-11', 'cat', 'Adhi', '081234567', '2021-11-10 20:30:04');
INSERT INTO "public"."appointment" VALUES ('3774c14e-4c15-4ce4-85d0-8f0239fbc81f', '2021-11-12', 'dog', 'Budi', '082198765', '2021-11-10 20:30:41');

-- ----------------------------
-- Primary Key structure for table appointment
-- ----------------------------
ALTER TABLE "public"."appointment" ADD CONSTRAINT "pk_appointment" PRIMARY KEY ("id");
